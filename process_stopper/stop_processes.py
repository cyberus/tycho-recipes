#!/usr/bin/env python2

# Tycho recipes - A collection of example scripts using the Tycho Python API
# Copyright (C) 2020 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pyTycho


# define dictionary for stopped processes and their PIDs
stopped = {}


def stop_non_essential_processes():
    """Stops all processes that are not essential to Windows"""

    essential_process_list = ["", "System", "explorer.exe", "services.exe",
                              "smss.exe", "svchost.exe", "taskmgr.exe",
                              "spoolsv.exe", "lsass.exe", "csrss.exe",
                              "winlogon.exe", "conhost.exe",
                              "taskhost.exe"]
    service = pyTycho.Tycho()

    def is_interesting(process_info):
        return process_info.file_name not in essential_process_list and \
            process_info.pid not in stopped.keys()

    def stop_process(process_info):
        proc = service.open_process_by_pid(process_info.pid)
        stopped[process_info.pid] = proc
        proc.pause()

    def stop_all_interesting_processes():
        return all(stop_process(p) for p in service.get_process_list()
                   if is_interesting(p))

    def check_processes():
        pl = service.get_process_list()
        return any(is_interesting(p) for p in pl)

    while check_processes():
        stop_all_interesting_processes()

def wait_for_input():
    """Asks the user when to resume halted processes."""
    while True:
        input = raw_input("Do you wish to resume all processes? -> yes/no \n")
        if not input.lower().strip() in ["yes", "y"]:
            print("Processes remain halted for now.")
        else:
            break

def main():
    stop_non_essential_processes()
    wait_for_input()

if __name__ == "__main__":
    main()

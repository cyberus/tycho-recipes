# Process stopping example

This Python script halts all processes which are not essential to Windows. This
can be used for example when dumping memory for a target PC.

## Usage

Run via `python stop_processes.py` to halt all processes und enter `yes` once
all processes can be resumed.

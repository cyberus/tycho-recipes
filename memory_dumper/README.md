# Ramdump example

This example creates a memory dump with Tycho to analyze in Volatility. The
`memdump.py` script automatically creates memory dump which can be analyzed.

The script needs to be supplied with a configuration file describing the target PC,
because the address ranges have to be reconfigured for every target PC.
This is because the address ranges of usable physical memory varies depending
on the machine. These regions can be located by using [RAMMap v1.52](https://docs.microsoft.com/en-us/sysinternals/downloads/rammap).
Download the program on your target PC and run the executable. The start and end
addresses can be found in the tab `Physical Ranges`. Insert these start and end
values into the `machine-file.json.example` file that is within this folder and
remove the `.example` extension. The existing file can be used as an example on
how to enter the values. Adjust the file according to the target PC by adding,
modifying or deleting entries.

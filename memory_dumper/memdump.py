#!/usr/bin/env python2

# Tycho recipes - A collection of example scripts using the Tycho Python API
# Copyright (C) 2020 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import argparse
import json
import os
import sys

import pyTycho

# Define dumpfile extension
dump_ext = ".raw"

def validate_params(arguments):
    """
    Checks all parameters before connecting to tycho-server

    Keyword arguments:
    arguments   :   the arguments parsed by the ArgumentParser
    """

    arguments = check_paths(arguments)
    return check_mem_map(arguments), arguments

def check_paths(arguments):
    """
    Checks if all necessary files exist.

    Keyword argument:
    arguments   :   the arguments parsed by the ArgumentParser
    """

    if not os.path.isfile(arguments.machine_file):
        sys.exit("{0} not found. Exiting...".format(arguments.machine_file))
    if arguments.dump_dir == '':
        arguments.dump_dir = os.getcwd()
    elif os.path.isdir(arguments.dump_dir):
        os.chdir(arguments.dump_dir)
    else:
        sys.exit("{0} not found. Exiting...".format(arguments.dump_dir))
    if not arguments.dump_name.endswith(dump_ext):
        arguments.dump_name += dump_ext
    if os.path.isfile(arguments.dump_name):
        if arguments.delete_old:
            if arguments.force:
                os.remove(arguments.dump_name)
            else:
                input = raw_input("A dumpfile named {0} already exists. Do you"
                                  " want to remove this file? y/n \n".
                                  format(arguments.dump_name))
                if input.lower() in ["y", "yes"]:
                    os.remove(arguments.dump_name)
                elif input.lower() in ["n", "no"]:
                    sys.exit("{0} has not been removed. Exiting...".
                             format(arguments.dump_name))
                else:
                    sys.exit("Invalid response. Exiting...")
        else:
            filename, ext = os.path.splitext(arguments.dump_name)
            counter = 0
            while True:
                filepath = "{0}{1}{2}".format(filename, counter, ext)
                if not os.path.isfile(filepath):
                    break
                counter += 1
            arguments.dump_name = filepath

    return arguments

def check_mem_map(arguments):
    """
    Checks if the physical ranges within mem_map are overlapping.

    Keyword argument:
    arguments   :   the arguments parsed by the ArgumentParser
    """
    machinefile = arguments.machine_file

    def check_range_size(item):
        return item["begin"] >= item["end"]

    def overlap(a, b):
        range_size = not check_range_size(a) and not check_range_size(b)
        overlaps = a["begin"] < b["end"] and b["begin"] < a["end"]
        return range_size and overlaps

    with open(machinefile, "r") as f:
        mem_map = json.load(f)
        for elem in mem_map:
            elem["begin"] = int(elem["begin"], base=16)
            elem["end"] = int(elem["end"], base=16)

        for i in range(1, len(mem_map)):
            if any(overlap(mem_map[i - 1], x) for x in mem_map[i:]):
                sys.exit("Memory map is overlapping or contains faulty values!"
                         " Please make sure all values in the machine file are"
                         " corrrect. Exiting...")

        return mem_map

def dump_ram(mem_map, arguments):
    """
    Reads RAM from physical ranges which are static for a specific target PC.

    Keyword arguments:
    mem_map     :   dictionary containing start and end addresses of the
                    physical pages that should be dumped
    arguments   :   the arguments parsed by the ArgumentParser
    """

    last_addr = 0
    service = pyTycho.Tycho()

    with open(arguments.dump_name, "w") as f:
        for counter, elem in enumerate(mem_map, 1):
            print("Padding in front of memory region {0}.".format(counter))
            padding_size = elem.get("begin") - last_addr
            f.write(b"\x00" * padding_size)
            print ("Dumping memory region {0}. This may take a while... \n".
                   format(counter))
            region_size = elem.get("end") - elem.get("begin")
            f.write(read_chunks(elem.get("begin"), region_size, service))
            last_addr = elem.get("end")

def read_chunks(address, region_size, service):
    """
    Reads memory in chunks and combines the results into one memory_chunk to
    avoid problems with requests which are too large.

    Keyword arguments:
    address         :   position to start reading from
    region_size     :   length to read
    service         :   pyTycho service
    """

    memory_chunk = ""
    chunk_size = 0x100000
    while (region_size > chunk_size):
        memory_chunk += service.read_physical(address, chunk_size)
        address += chunk_size
        region_size -= chunk_size

    if region_size > 0:
        memory_chunk += service.read_physical(address, region_size)

    return memory_chunk

def parse_args():
    """Parses call parameters for correct usage."""

    parser = argparse.ArgumentParser(description='This script dumps physical \
                                     memory from a target pc')
    parser.add_argument('--force', '-f', action='store_true', help='set \
                        --force to skip any dialoges.')
    parser.add_argument('--dump_name', metavar='dump_name',
                        default='memorydump{}'.format(dump_ext), nargs='?',
                        help='set --dump_name <FILE> to define what the \
                        resulting dumpfile should be called.')
    parser.add_argument('--dump_dir', metavar='dump_dir', default=os.getcwd(),
                        nargs='?', help='set --dump_dir <DIRECTORY> to define \
                        where to save the dumpfile.')
    parser.add_argument('--delete_old', action='store_true', help='set \
                        --delete_old to be asked if you want to delete old \
                        memory dumps within the respective directory.')
    parser.add_argument('--machine_file', metavar='machine_file',
                        default='machine-file.json', nargs='?',
                        help='set --machine_file <FILE> to specify a file \
                        from which to extract the memory map.')

    return parser.parse_args()

def main():
    arguments = parse_args()
    mem_map, arguments = validate_params(arguments)
    dump_ram(mem_map, arguments)

if __name__ == "__main__":
    main()

#!/usr/bin/env python2

# Tycho-based System Monitor. Monitors processes with the help of Tycho and
# Elasticsearch.
#
# Copyright (C) 2018-2019  Alexander Rausch <alexander.rausch@dcso.de>
# Copyright (C) 2018-2019  Florian Pester
#    <florian.pester@cyberus-technology.de>
# Copyright (C) 2019       Philipp Barthel
#    <philipp.barthel@cyberus-technology.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Tycho-based System Monitor. Monitors processes with the help of Tycho and
Elasticsearch.

This example showcases how Tycho can be used to monitor different processes and
how this can be integrated with an Elastic Stack.
This example comes with a docker-compose file to setup the Elastic Stack and a
Kibana dashboard to show the output in a nice way.

NOTES:
This script may fail when a child process spawned by the parent gets
assigned a PID from a previous child.
It is possible that a child process which terminates itself very quickly could
be missed, resulting in a print message repeating "Child process not running
yet...".
"""

import argparse
import datetime
import json
import os
import subprocess
import sys
import time
import uuid

from chardet.universaldetector import UniversalDetector

import pyTycho
from pyTycho.syscall_interpreter import SupportedWindowsVersions, \
                                        SystemCallInterpretationFactory
from pyTycho.syscall_interpreter import value_tag, ptr_tracked_tag

# Default Environment
LOGSTASH_DIR = os.environ.get("LOGSTASH_DIR", "./logstash/data/in")
PYTHON_BIN = os.environ.get("PYTYCHO_BIN", "/usr/bin/python2")

# Define Windows version of the target system
win_version = SupportedWindowsVersions.win7_24150
pyTycho.Tycho.windows_version = win_version

syscall_overrides = {
    pyTycho.Tycho.syscalls.NtOpenKeyEx :
        ("NtOpenKeyEx",
            [
                ("OUT", "HANDLE*", "pKeyHandle"),
                ("IN", "ACCESS_MASK", "DesiredAccess"),
                ("IN", "OBJECT_ATTRIBUTES*", "ObjectAttributes"),
                ("IN", "ULONG", "OpenOptions"),
            ]
         )
    }
pyTycho.syscall_interpreter.syscall_signatures[win_version].update(
                                                    syscall_overrides)

# Global list of child process handles to ensure a process stays paused until
# the subscript can attach itself to this process
child_handle_list = []

# List of child_pids to ensure get_child_pid() finds the correct child process
childprocess_id_list = []

# List of subprocesses to exit the main script not before all subscripts
# have exited
subscript_list = []

# Define pointer types which are followed automatically
pointer_tracking = ["OBJECT_ATTRIBUTES*", "UNICODE_STRING*",
                    "RTL_USER_PROCESS_PARAMETERS*"]

# Define custom actions on types
# extract_wstring reads from the UNICODE_STRING object to extract the handler
custom_handlers = [
    ("UNICODE_STRING", lambda proc, obj: extract_wstring(proc, obj))
]

logfile_name = ""
process_name = ""


def decode_windows_data(data):
    """
    Decodes any data given by the system call interpretation and auto-detects
    the used encoding. The decoded data is encoded in ASCII before returned to
    ensure correct functionality of JSON.dump()

    Keyword arguments:
    data    : encoded String
    """
    detector = UniversalDetector()
    detector.feed(data)
    detector.close()
    if detector.done:
        encoding = detector.result.get("encoding")
        if encoding is None:
            return ""
        if encoding == 'Windows-1252':
            # data which is detected to be Windows-1252 encoded can be
            # discarded as it is irrelevant and crashes JSON.dump().
            return ""
        decoded_data = data.decode(encoding)
        ascii_data = decoded_data.encode(encoding='ascii', errors='ignore')
        return ascii_data
    else:
        return ""

def read_buffer(proc, value):
    """
    Extracts a pointer and a length to read from the given value.

    Keyword arguments:
    proc    : Tycho process
    value   : dictionary expected to contain buffer and length key
    """
    pointer = value["Buffer"][value_tag]
    length = value["Length"][value_tag]
    return chunk_read(pointer, length, proc)

def extract_wstring(proc, object_representation):
    """
    Reads the buffer in a UNICODE_STRING object and
    interprets content of any encoding. The content is then encoded with ascii
    and appended to the given object. This function is intended to be used in
    the custom interpretation handler to define the handler.

    Keyword arguments:
    proc                  : Tycho process
    object_representation : dictionary with type and value
    """
    value = object_representation[value_tag]
    content = read_buffer(proc, value)
    object_representation[value_tag]["content"] = decode_windows_data(content)
    return object_representation

def extract_write_file_buffer(proc, parameters):
    """
    Reads the buffer passed to the NtWriteFile system call and
    interprets content of any encoding. The content is then encoded with ascii
    and appended to the given parameters.

    Keyword arguments:
    proc       : Tycho process
    parameters : Tycho syscall parameters representation
    """
    content = read_buffer(proc, parameters)
    parameters["Buffer"][ptr_tracked_tag] = {
        "content": decode_windows_data(content)
    }
    return parameters

def log_event(ev):
    """
    Logs the given event in the specified logfile.

    Keyword arguments:
    ev  : dictionary
    """
    with open(logfile_name, "ab") as f:
        json.dump(ev, f, ensure_ascii=False, indent=None)
        f.write("\n")

def chunk_read(pointer, length, process):
    """
    Reads memory in chunks and combines the results into one buffer to avoid
    problems with too large requests. Includes a sanity check to prevent
    reading from invalid addresses.

    Keyword arguments:
    pointer : position to start reading from
    length  : length to read
    process : Tycho process
    """
    if pointer == 0 or pointer == 0xFFFFFFFFFFFFFFFF:
        return ""
    buffer = ""
    buffer_temp = ""
    chunk_size = 0x400
    while (length > chunk_size):
        buffer_temp = process.read_linear(pointer, chunk_size)
        pointer += chunk_size
        buffer += buffer_temp
        length -= chunk_size

    buffer_temp = process.read_linear(pointer, length)
    buffer += buffer_temp
    return buffer

def get_timestamp():
    """Returns timestamp formatted as HH:MM:SS."""
    return datetime.datetime.now().strftime("%H:%M:%S")

def get_child_pid(parent_pid, childprocess_id_list):
    """
    Determines the PID of a spawned process by looking for entries in the
    process list that have the current PID as a parent.

    Keyword arguments:
    parent_pid           : PID from this script's process
    childprocess_id_list : list of PIDs of spawned processes by this process
    """
    pl = service.get_process_list()
    for test_child in pl:
        if test_child.parent_pid == parent_pid and test_child.pid not in \
                                                   childprocess_id_list:
            childprocess_id_list.append(test_child.pid)
            return test_child.pid
    return None

def parse_args():
    """Parses call parameters for correct usage."""
    parser = argparse.ArgumentParser(description='This script traces \
                                     whitelisted systemcalls of the given \
                                     process and its child processes')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--pid', '-p', metavar='pid', type=int, nargs='?',
                       help='set --pid to start this script with a pid of \
                       a process')
    group.add_argument('--name', '-n', metavar='name', default='', nargs='?',
                       help='set --name to start this script with a name of \
                       a process')
    parser.add_argument('--level', '-l', metavar='level', type=int, default=0,
                        nargs='?', help=argparse.SUPPRESS)

    return parser.parse_args()

def handle_syscall_event(fact, proc, ev, process_name, level,
                         childprocess_id_list):
    """
    Prints information about the name, return value, input and output
    parameters of the system call.

    Keyword arguments:
    fact                 : system call interpretation factory
    proc                 : Tycho process
    ev                   : syscall breakpoint event
    process_name         : this process's name
    level                : depth-level of spawned processes
    childprocess_id_list : list of PIDs of spawned processes by this process
    """
    with fact.syscall_interpretation(ev,
                                     pointer_tracking=pointer_tracking,
                                     custom_handlers=custom_handlers) \
                                     as interpreter:
        name = interpreter.get_syscall_name()
        in_parameters = interpreter.get_parameters()
        if name == "NtWriteFile":
            in_parameters = extract_write_file_buffer(proc, in_parameters)
        out_parameters = interpreter.execute()
        syscall_output = {"name": name,
                          "process": process_name,
                          "return_value": interpreter.get_return_value(),
                          "before": in_parameters,
                          "after": out_parameters}
        print("[{0}]:\tProcess {1:20} (PID: {2:4}, Level: {3}) used  \t"
              "syscall:{4}".format(get_timestamp(), process_name,
                                   proc.get_pid(), level, name))
        if name == "NtTerminateProcess":
            print("[{0}]:\tProcess {1:20} (PID: {2}, Level: {3}) has been"
                  " terminated".format(get_timestamp(), process_name,
                                       proc.get_pid(), level,))
            for p in subscript_list:
                p.wait()
            exit(0)
        elif name == "NtCreateUserProcess":
            child_pid = get_child_pid(proc.get_pid(), childprocess_id_list)
            if child_pid is not None:
                child_process = service.open_process_by_pid(child_pid)
                child_process.pause()
                child_handle_list.append(child_process)
                sub = subprocess.Popen([PYTHON_BIN, sys.argv[0], "--pid",
                                        str(child_pid), "-l", str(level+1)])
                subscript_list.append(sub)
            else:
                print('No matching Child PID was found in the process-list.'
                      ' Therefore, no subscript was started.')
        log_event(syscall_output)

if __name__ == "__main__":
    arguments = parse_args()
    process_name = arguments.name
    this_pid = arguments.pid
    level = arguments.level

    logfile_name = "%s/%s_%s.json" % (LOGSTASH_DIR, process_name,
                                      str(uuid.uuid4()))

    service = pyTycho.Tycho()

    if level == 0:
        process = service.open_process_by_name(process_name)
        process.pause()
        this_pid = process.get_pid()
    else:
        try:
            process = service.open_process_by_pid(this_pid)
            process.pause()
            while not process.is_running():
                time.sleep(0.5)
                print('[{}]:\tChild process                '
                      'not running yet...'.format(get_timestamp()))

            process_name = process.get_info().file_name

        except PidNotAvaiable as e:
            print(e)
            exit("[{0}]:\tCannot find running process with PID: {1}.".
                 format(get_timestamp(), this_pid))

    while not process.is_running():
        print('[{0}]:\tProcess {1:20} not running yet...'.
              format(get_timestamp(), process_name))
        time.sleep(0.5)

    print('\tProcess-ID is:\t\t{}'.format(this_pid))
    print('\tProcess-name is:\t{}'.format(process_name))
    print('\tLevel is:\t\t{}'.format(level))

    # The following syscall whitelist contains a variety of system calls that
    # could prove useful depending on what programm is tracked. Uncomment,
    # comment or add system calls depending on need.
    syscall_whitelist = (
        # pyTycho.Tycho.syscalls.NtAdjustPrivilegesToken,
        pyTycho.Tycho.syscalls.NtCreateUserProcess,
        pyTycho.Tycho.syscalls.NtCreateProcess,
        # pyTycho.Tycho.syscalls.NtCreateProcessEx,
        # pyTycho.Tycho.syscalls.NtCreateThread,
        # pyTycho.Tycho.syscalls.NtCreateMutant,
        # pyTycho.Tycho.syscalls.NtCreateNamedPipeFile,
        pyTycho.Tycho.syscalls.NtCreateKey,
        pyTycho.Tycho.syscalls.NtOpenKey,
        pyTycho.Tycho.syscalls.NtOpenKeyEx,
        # pyTycho.Tycho.syscalls.NtClose,
        pyTycho.Tycho.syscalls.NtSetValueKey,
        pyTycho.Tycho.syscalls.NtOpenFile,
        # pyTycho.Tycho.syscalls.NtSetInformationFile,
        # pyTycho.Tycho.syscalls.NtDeviceIoControlFile,
        # pyTycho.Tycho.syscalls.NtQueryAttributesFile,
        pyTycho.Tycho.syscalls.NtTerminateProcess,
        # pyTycho.Tycho.syscalls.NtLoadDriver,
        # pyTycho.Tycho.syscalls.NtUnloadDriver,
        pyTycho.Tycho.syscalls.NtWriteFile,
        pyTycho.Tycho.syscalls.NtCreateFile)

    breakpoint = process.get_syscall_breakpoint()
    for syscall in syscall_whitelist:
        breakpoint.add_syscall_whitelist(syscall)
    breakpoint.enable()

    factory = SystemCallInterpretationFactory(process)

    while True:
        process.resume()
        event = process.wait_for_breakpoint()
        if event.event_category == event.SYSCALL_BP:
            syscall_name = handle_syscall_event(factory, process, event,
                                                process_name, level,
                                                childprocess_id_list)

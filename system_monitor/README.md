# System Monitor Example

This example shows a Tycho-based system monitor. Use the `docker-compose.yml`
file to start an ELK (Elasticsearch database, Logstash, Kibana dashboard)
stack and the `syscall_process_tracing.py` script to attach to any process and
its spawned child-processes running on a Windows target. To start the script
use `python syscall_process_tracing.py --name <process-name>`.
This script's syscall whitelist can be adjusted to track different system calls.
Check the Tycho documentation for what system calls are supported.

Once you have started a few processes on the Windows side you can watch their
activity in Kibana by pointing your browser to `localhost:5601`.

If you want to see a nice dashboard import the `json` file located in the
`kibana_objects` folder. This dashboard shows for example a system call heat
map and system calls over time among other visualizations. To import this
dashboard go to `Management` -> `Saved Objects` -> `Import` within Kibana.

To filter the logs produced by the python script, SIGMA-rules can be used.
Download the SIGMA repository at `https://github.com/Neo23x0/sigma`. To compile
rules into a format that can be imported into Kibana, run `python3 sigmac -t
kibana -c logstash-defaultindex -O keyword_field="" <path/to/yml-rule>.yml
--output <rule-name>.json` in a terminal.

#!/usr/bin/env python2

# Tycho recipes - A collection of example scripts using the Tycho Python API
# Copyright (C) 2020 Sebastian Manns, Cyberus Technology GmbH
# <sebastian.manns@cyberus-technology.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import binascii
import collections
from datetime import datetime
import json
import struct
import socket
import sys
import time
import pyTycho
from pyTycho.syscall_interpreter import SystemCallInterpretationFactory,\
                                        SupportedWindowsVersions

"""Store registered IOCTL handlers.

Handler functions for supported IOCTL codes can be registered by mapping
the numeric value to the corresponding handler function.

Example: IOCTL_HANDLER[IOCTL_NUM['AFD_Connect']] = handle_afd_connect
"""

IOCTL_NUM = {
    "AFD_Connect": 0x12007,
    "AFD_Send":    0x1201F,
    "AFD_Recv":    0x12017

}
IOCTL_HANDLER = {}

# Define Windows version of the target system
win_version = SupportedWindowsVersions.win7_24150
pyTycho.Tycho.windows_version = win_version

Ipv4Connection = collections.namedtuple("Ipv4Connection", ["address", "port"])

# Contains pointer and size from the array of transmitted data_set
BufferDescriptor = collections.namedtuple("BufferDescriptor", ["pointer",
                                                               "size"])

IoctlParameters = collections.namedtuple("IoctlParameters",
                                         ["input_buffer_address",
                                          "input_buffer_length",
                                          "io_status_block",
                                          "file_handle"])

# Mapping between a file handle and the corresponding Ipv4Connection
connection_dict = {}


class UnsupportedAddressFamily(Exception):
    """This exception is raised if the address family is not AF_INET (IPv4)."""


def parse_sockaddr(sockaddr_buffer):
    """
    Returns address and port described by the buffer.

    The buffer needs to be of the `SOCKADDR` type as defined in Windows:
    https://docs.microsoft.com/en-us/windows/win32/winsock/sockaddr-2

    Note that currently, only AF_INET (IPv4) is supported.

    Keyword arguments:
    sockaddr_buffer : Buffer containing a complete SOCKADDR structure

    Returns:
    Ipv4Connection

    Raises:
    UnsupportedAddressFamily : the connection is not AF_INET (IPv4)
    """
    sa_family = struct.unpack("H", sockaddr_buffer[0:2])[0]
    if sa_family != socket.AF_INET:
        raise UnsupportedAddressFamily("Address family {} is not supported"
                                       .format(sa_family))
    ip = socket.inet_ntop(sa_family, sockaddr_buffer[4:8])
    port = struct.unpack(">H", sockaddr_buffer[2:4])[0]
    return Ipv4Connection(ip, port)


def parse_afd_connect(afd_connect_info):
    """
    Extracts the remote address passed to IOCTL_AFD_CONNECT.

    The `InputBuffer` parameter of this system call points to a structure
    of type `AFD_CONNECT_INFO`. This structure contains a member
    `RemoteAddress` of type `SOCKADDR` at offset 24, which is passed
    to `parse_sockaddr` to extract the IP address and port.

    Keyword arguments:
    afd_connect_info : InputBuffer of NtDeviceIoControlFile

    Returns:
    Ipv4Connection
    """
    sockaddr_member = afd_connect_info[24:]
    return parse_sockaddr(sockaddr_member)


def parse_pafd_wsabuf(pafd_wsabuf, proc):
    """
    Returns the pointer to the transmitted data buffer.

    The buffer needs to be of the `WSABUF` type as defined in Windows:
    https://docs.microsoft.com/en-us/windows/win32/api/ws2def/ns-ws2def-wsabuf

    Keyword arguments:
    pafd_wsabuf : Pointer to a `WSABUF` struct
    proc        : Tycho process

    Returns:
    bytearray
    """

    return struct.unpack("<Q", proc.read_linear(pafd_wsabuf+8, 8))[0]


def get_ioctl_parameters(in_params, out_params):
    """
    Returns the address and length from the Inputbuffer, the address from
    IoStatusBlock and the FileHandle from NtDeviceIoControlFile.

    Keyword arguments:
    in_params  : input parameters from NtDeviceIoControlFile
    out_params : output parameters from NtDeviceIoControlFile

    Returns:
    IoctlParameters
    """
    input_buffer_address = in_params["InputBuffer"]["value"]
    input_buffer_length = in_params["InputBufferLength"]["value"]
    io_status_block = out_params["IoStatusBlock"]["value"]
    file_handle = in_params["FileHandle"]["value"]
    return IoctlParameters(input_buffer_address, input_buffer_length,
                           io_status_block, file_handle)


def parse_afd_send_recv(ioctl_parameters, proc):
    """
    Extract the pointer to the transmitted data passed to IOCTL AFD_SEND
    or AFD_RECV and the size of transmitted data from IoStatusBlock.

    The `InputBuffer` parameter of this system call points to a structure
    of the type `AFD_SEND_INFO` or `AFD_RECV_INFO`. This structure contains
    a member `BufferArray` of type `PAFD_WSABUF` at offset 0, which is passed
    to `parse_pafd_wsabuf` to extract the transmitted data.

    The IoStatusBlock structure has a member `Information` at offset 8,
    which contains the size of the transmitted data.
    https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/ns-wdm-_io_status_block


    Keyword arguments:
    ioctl_parameters : Contains the InputBuffer pointer + length,
                       pointer to IoStatusBlock and the file handle
                       of NtDeviceIoControlFile system call
    proc             : Tycho process

    Returns:
    BufferDescriptor
    """
    afd_send_recv_info = proc.read_linear(ioctl_parameters.input_buffer_address,
                                          ioctl_parameters.input_buffer_length)
    pafd_wsabuf = struct.unpack_from("<Q", afd_send_recv_info, 0)[0]
    buf_pointer = parse_pafd_wsabuf(pafd_wsabuf, proc)

    buf_size = struct.unpack("<L", proc.read_linear(
                             ioctl_parameters.io_status_block + 8, 4))[0]
    return BufferDescriptor(buf_pointer, buf_size)


def generate_json(file_name, data_set):
    """
    Generates a JSON file for non-binary data
    (transmission time, pointer, transmitted data).

    Keyword arguments:
    file_name : Json file
    data_set  : transmitted Data information

    Returns:
    n/a
    """
    try:
        with open(file_name, "r") as json_file:
            data = json.load(json_file)
    except IOError:
        data = []

    data.append(data_set)

    with open(file_name, "w+") as json_file:
        json.dump(data, json_file, indent=2)


def dump_data(BufferDescriptor, connection, proc, direction):
    """
    Create a JSON file for the monitoring process and each direction.
    The name of the created JSON file consists of the name of the
    process, the remote IP address, the port and the direction
    (send or receive).

    Like this: `client.exe_127.0.0.1_8080_send.json`
               `client.exe_127.0.0.1_8080_recv.json`

    The transmitted data is recorded with a timestamp, pointer from the
    BufferDescriptor, message size and written to the JSON file.
    If a JSON file with the same name already exists, new data is added
    to this JSON file.

    For data that cannot be decoded a `process_name_binary_data_send.bin` or
    `process_name_binary_data_recv.bin` file is created.

    Like this: `client.exe_127.0.0.1_8080_send.bin`
               `client.exe_127.0.0.1_8080_recv.bin`

    Keyword arguments:
    BufferDescriptor : Contains pointer and size to the transmitted data
    connection       : Ipv4Connection
    proc             : Tycho process
    direction        : send or receive

    Returns:
    n/a
    """
    current_timestamp = datetime.now().strftime('%H:%M:%S')
    file_name = "{}_{}_{}_{}.json".format(sys.argv[1], connection.address,
                                          connection.port, direction)
    binary = "{}_{}_{}_{}.bin".format(sys.argv[1], connection.address,
                                      connection.port, direction)
    dump = proc.read_linear(BufferDescriptor.pointer, BufferDescriptor.size)

    data_set = {
        "transmission time": current_timestamp,
        "pointer": hex(BufferDescriptor.pointer),
        "message size": BufferDescriptor.size
    }

    try:
        data = dump.decode("utf8")
        print("{} {} byte Unicode data: {}".format(direction,
                                                   BufferDescriptor.size,
                                                   data))
    except UnicodeDecodeError:
        data = "binary"
        print("{} {} byte binary data: {}".format(direction,
                                                  BufferDescriptor.size,
                                                  binascii.hexlify(dump[:16])))
        with open(binary, "a") as binary_file:
            binary_file.write(dump)

    data_set["data"] = "{}".format(data)
    generate_json(file_name, data_set)


def handle_afd_connect(proc, ioctl_parameters):
    """
    Handles an event to establish a network connection using the syscall
    NtDeviceIoControlFile with the IOCTL `AFD_Connect`.
    Creates mapping between a file handle and the corresponding Ipv4Connection
    And prints information (IP address, port, application protocol)

    Keyword arguments:
    proc             : Tycho process
    ioctl_parameters : Contains the InputBuffer pointer + length,
                       pointer to IoStatusBlock and the file handle
                       of NtDeviceIoControlFile system call
    Returns:
    n/a

    Execption:
    UnsupportedAddressFamily : the connection is not AF_INET (IPv4)
    """
    try:
        input_buffer = proc.read_linear(ioctl_parameters.input_buffer_address,
                                        ioctl_parameters.input_buffer_length)
        ip_port = parse_afd_connect(input_buffer)
        connection_dict[ioctl_parameters.file_handle] = ip_port
        try:
            socket_type = socket.getservbyport(ip_port.port)
        except socket.error:
            socket_type = "unknown service type"
        print("Open a connection to IP: {}:{} ({})".format(ip_port.address,
                                                           ip_port.port,
                                                           socket_type))

    except UnsupportedAddressFamily as e:
        print(e.message)


IOCTL_HANDLER[IOCTL_NUM['AFD_Connect']] = handle_afd_connect


def handle_transmitted_data(proc, ioctl_parameters, direction):
    """
    Handels NtDeviceIoControlFile with IOCTL codes AFD_SEND and AFD_RECV by
    dumping unicode and binary data into respective JSON files.
    See dump_data() for more details.

    Keyword arguments:
    proc             : Tycho process
    ioctl_parameters : Contains the InputBuffer pointer + length,
                       pointer to IoStatusBlock and the file handle
                       of NtDeviceIoControlFile system call
    direction        : send or receive

    Returns:
    n/a
    """
    # If the connection was not recorded, the data is ignored, because
    # there is no way to associate it with an IP/port combination.
    if ioctl_parameters.file_handle not in connection_dict:
        print("Transmission on a connection that is not monitored. "
              "Make sure to attach to the process as early as possible "
              "ideally, before any connections are opened that are later "
              "used for sending/receiving data).")
        return

    buffer = parse_afd_send_recv(ioctl_parameters, proc)
    dump_data(buffer, connection_dict[ioctl_parameters.file_handle],
                                      proc, direction)


def handle_afd_send(proc, ioctl_parameters):
    """
    Handles an event when the process sends data using the syscall
    NtDeviceIoControlFile with the IOCTL `AFD_Send`.

    Keyword arguments:
    proc             : Tycho process
    ioctl_parameters : Contains the InputBuffer pointer + length,
                       pointer to IoStatusBlock and the file handle
                       of NtDeviceIoControlFile system call

    Returns:
    n/a
    """
    handle_transmitted_data(proc, ioctl_parameters, "send")


IOCTL_HANDLER[IOCTL_NUM['AFD_Send']] = handle_afd_send


def handle_afd_recv(proc, ioctl_parameters):
    """
    Handles an event when the process receive data using the syscall
    NtDeviceIoControlFile with the IOCTL `AFD_Recv`.

    Keyword arguments:
    proc             : Tycho process
    ioctl_parameters : Contains the InputBuffer pointer + length,
                       pointer to IoStatusBlock and the file handle
                       of NtDeviceIoControlFile system call

    Returns:
    n/a
    """
    handle_transmitted_data(proc, ioctl_parameters, "recv")


IOCTL_HANDLER[IOCTL_NUM['AFD_Recv']] = handle_afd_recv


def handle_syscall_event(fact, ev, proc):
    """
    This function handles two system calls:
    - NtDeviceIoControlFile:
        - with IOCTL `AFD_Connect`:
          get remote address information (IP, port, protocol)
        - with IOCTL `AFD_Send` or `AFD_RECV`:
          get transmitted data

    - NtTerminateProcess: Terminate the script

    Keyword arguments:
    fact : System call interpretation factory
    ev   : Syscall breakpoint event
    proc : Tycho process

    Returns:
    n/a
    """
    with fact.syscall_interpretation(ev) as interpreter:
        in_parameters = interpreter.get_parameters()
        name = interpreter.get_syscall_name()
        out_parameters = interpreter.execute()

    if name == "NtDeviceIoControlFile":
        ioctl = in_parameters["IoControlCode"]["value"]
        if ioctl not in IOCTL_HANDLER:
            return
        IOCTL_HANDLER[ioctl](proc, get_ioctl_parameters(in_parameters,
                                                        out_parameters))

    if name == "NtTerminateProcess":
        exit(0)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("Usage: python {} <process_name>".format(sys.argv[0]))

    service = pyTycho.Tycho()
    process = service.open_process_by_name(sys.argv[1])
    process.pause()
    while not process.is_running():
        print("process not running")
        time.sleep(0.5)

    breakpoint = process.get_syscall_breakpoint()
    breakpoint.add_syscall_whitelist(pyTycho.Tycho.syscalls.NtDeviceIoControlFile)
    breakpoint.add_syscall_whitelist(pyTycho.Tycho.syscalls.NtTerminateProcess)
    breakpoint.enable()
    factory = SystemCallInterpretationFactory(process)

    while True:
        process.resume()
        event = process.wait_for_breakpoint()
        if event.event_category == event.SYSCALL_BP:
            handle_syscall_event(factory, event, process)

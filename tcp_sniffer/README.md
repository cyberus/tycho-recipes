# TCP sniffer

This example uses the input buffer of `NtDeviceIoControlFile` to get the
following information:

* Port and remote address
* The transferred data

As soon as the process has established a connection to the server, a message is
printed with the remote address and port, which gets resolved to its application
protocol, if possible.

When the process sends or receives data, a `JSON` file  for each direction is
created in the same folder.
The name of the `JSON` file is made up of `process name`, the `remote IP`,
the `port` and the direction (send or receive).
In the file, the transmitted data, the pointer to the buffer, the size of
transmitted data and a timestamp is stored.

Example: `nc64.exe_127.0.0.1_21_send.json`
         `nc64.exe_127.0.0.1_21_recv.json`

If the transmitted data cannot be decoded, an additional .bin file is created
for this binary data.

Example: `nc64.exe_binary_data_send.bin`
         `nc64.exe_binary_data_recv.bin`

## Usage

Run via `python tcp_sniffer.py <process-name>`.  

Example: `python tcp_sniffer.py nc64.exe`

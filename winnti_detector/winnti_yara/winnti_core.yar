rule winnti_core : winnti_process {
meta:
    hash = "0a9d8f2cc581e3ca5f23458d644455634a17ea010c55ed23cc4db59a26cd667d"
    author = "Philipp Barthel @ Cyberus Technology"

strings:
    $s1 = "[] Load Dirver Result = %u/%#X." ascii fullword
    $s2 = "\\Registry\\Machine\\System\\CurrentControlSet\\Services\\%s" ascii fullword
    $s3 = "-k netsvcs" ascii fullword
    $s4 = "\\temp\\dsefix.exe" ascii fullword
    $s5 = "Global\\177796B2FD36AFE9A6A43DB53AA7EA14" ascii fullword
    $s6 = "SOFTWARE\\Microsoft\\HTMLHelp" ascii fullword
    $s7 = "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)" ascii fullword
    $s8 = "CONNECT %s:%d HTTP/1.0" ascii fullword
    $s9 = "dick.mooo.com" ascii fullword
    $s10 = "208.67.222.222" ascii fullword
    $s11 = "SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}" ascii fullword

condition:
    any of them
}
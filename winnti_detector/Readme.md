# Winnti detector example

This example checks all svchost.exe instances running on the target PC for
injected [Winnti](https://securelist.com/winnti-more-than-just-a-game/37029/) code using Yara rules.
The Winnti malware is a trojan, which gives the attacker remote access to the
infected system. Winnti has been around since 2012 and has been used to steal
company data from various large companies mostly situated in Germany.

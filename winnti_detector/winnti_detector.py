#!/usr/bin/env python2

# Automatic Tycho-based Winnti detection script.
#
# Copyright (C) 2020 Philipp Barthel, Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import time
import yara

from datetime import datetime

import pyTycho
from pyTycho.syscall_interpreter import SupportedWindowsVersions, \
                                        SystemCallInterpretationFactory

# Define Windows version of the target system
win_version = SupportedWindowsVersions.win7_24150
pyTycho.Tycho.windows_version = win_version

# Define YARA signature
rules = yara.compile('winnti_yara/winnti_core.yar')

# This exception is raised if the script is called with a specific PID but this
# PID does not belong to an instance of svchost.exe
class ProcessNotAvailable(Exception):
    pass

# This exception is raised if the memory address of a VAD node is invalid
class InvalidMemoryAddress(Exception):
    pass

def read_mem_chunks(address, region_size, process):
    """
    Reads memory in chunks and combines the results into one buffer to avoid
    problems with too large requests. Includes a sanity check to prevent
    reading from invalid addresses.

    Keyword arguments:
    address     : address to start reading from
    region_size : amount of bytes to read
    process     : Tycho process
    """
    def memory_chunk_iterator(address, region_size, process,
                              chunk_size=0x100000):
        while region_size > 0:
            chunk = min(chunk_size, region_size)
            yield process.read_linear(address, chunk)
            address += chunk
            region_size -= chunk

    if address == 0 or address == 0xFFFFFFFFFFFFFFFF:
        raise InvalidMemoryAddress("Memory address {} is not valid.".
                                   format(address))
    return "".join(memory_chunk_iterator(address, region_size, process))

def get_infected_svchosts(service, specified_pid):
    """
    Checks VAD trees of svchost.exe processes for malicious Winnti code until
    Winnti has been detected or all trees are checked and returns the result.

    Keyword arguments:
    service         : pyTycho service
    specified_pid   : PID of a single svchost.exe or None if -p is not set
    """

    def dump_vad_node_mem(proc, start_vpn, end_vpn):
        """
        Dumps and returns virtual address space of a process's VAD node.

        Keyword arguments:
        proc        : pyTycho process
        start_vpn   : first virtual page number of the VAD node
        end_vpn     : last virtual page number of the VAD node
        """
        start_offset = (start_vpn * 4096)
        end_offset = ((end_vpn + 1) * 4096)
        amount_of_bytes = end_offset - start_offset
        vad_node_memory = read_mem_chunks(start_offset, amount_of_bytes, proc)
        return vad_node_memory

    def is_infected_vad_node(vad_node, proc):
        """
        Iterates over a list of VAD nodes to dump respective virtual address
        space to match with Yara rules. Returns true if a matching rule was
        found.

        Keyword arguments:
        vad_node    : pyTycho VAD node
        proc:       : pyTycho process
        """
        vad_node_memory = dump_vad_node_mem(proc, vad_node.start_vpn,
                                            vad_node.end_vpn)
        return rules.match(data=vad_node_memory)

    def is_infected_process(service, pid):
        """
        Checks a process and returns the result of the corresponding Yara
        checks.

        Keyword arguments:
        service : pyTycho service
        pid     : process ID
        """
        with service.open_process_by_pid(long(pid)) as proc:
            while not proc.is_running():
                time.sleep(0.5)
            proc.pause()
            print("[{0}]: Dumping virtual address space of process {1} ... "
                  "this could take a while \n".format(datetime.now().strftime(
                                                      "%H:%M:%S"), pid))
            vad_list = [o for o in proc.get_vad_list() if not
                        o.filename.endswith(".dll")]
            return any(is_infected_vad_node(vad_node, proc) for vad_node
                       in vad_list)

    def find_svchosts(pslist):
        """
        Returns a list of PIDs to scan for an infection.
        By default, all processes named "svchost.exe" are candidates. This
        selection can be further narrowed down by passing a specific PID to
        the script.

        Keyword argument:
        pslist          : list of processes currently running on the target
                          machine
        """
        def find_possible_victims():
            return [proc.pid for proc in pslist
                    if proc.file_name == "svchost.exe"]


        svc_list = [specified_pid] if specified_pid else find_possible_victims()

        # Winnti preferably infects svchost.exe with arguments '-k netsvcs',
        # which in all tested cases had a PID between 900 and 999. Checking for
        # the arguments would be better but has a lot of overhead and would be
        # difficult to implement. Rearranging PIDs in svc_list for scanning is
        # good enough to lower detection-time.
        if not svc_list:
            raise ProcessNotAvailable("No process with PID: {0} found".
                                      format(pid))

        return sorted(svc_list, reverse=True, key=lambda
                      item: 900 <= item <= 999)


    svc_list = find_svchosts(service.get_process_list())

    return next((pid for pid in svc_list
                 if is_infected_process(service, pid)), -1)


def parse_args():
    """Parses call parameters for correct usage."""

    parser = argparse.ArgumentParser(
        description='This script uses Tycho to check if the target PC has been \
        infected with Winnti malware.'
        )
    parser.add_argument(
        '--pid', '-p', metavar='PID', action='store', help='specify a process \
        ID to check for injected Winnti code'
        )

    return parser.parse_args()

def main():
    arguments = parse_args()
    service = pyTycho.Tycho()

    infected_pid = get_infected_svchosts(service, arguments.pid)
    if infected_pid > 0:
        print("[{0}]: ************ WINNTI DETECTED IN PID {1} ************".
              format(datetime.now().strftime("%H:%M:%S"), infected_pid))
    else:
        print("[{0}]: NO WINNTI FOUND! Exiting..".format(datetime.now().
                                                         strftime("%H:%M:%S")))

if __name__ == "__main__":
    main()

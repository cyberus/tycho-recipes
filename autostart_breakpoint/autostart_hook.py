# Tycho recipes - A collection of example scripts using the Tycho Python API
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pprint
import sys
import time

import idc
import ida_dbg
import ida_idaapi
import ida_idd
import ida_kernwin

import pyTycho
from pyTycho.syscall_interpreter import SupportedWindowsVersions, \
                                        SystemCallInterpretationFactory
from pyTycho.syscall_interpreter import value_tag, ptr_tracked_tag

# Define Windows version of the target system
win_version = SupportedWindowsVersions.win7_24150
pyTycho.Tycho.windows_version = win_version

# This system call signature is not yet present in the interpreter,
# so we add it manually.
syscall_overrides = {
    pyTycho.Tycho.syscalls.NtOpenKeyEx :
        ( "NtOpenKeyEx",
            [
                ("OUT", "HANDLE*", "pKeyHandle"),
                ("IN", "ACCESS_MASK", "DesiredAccess"),
                ("IN", "OBJECT_ATTRIBUTES*", "ObjectAttributes"),
                ("IN", "ULONG", "OpenOptions"),
            ]
        )
}
pyTycho.syscall_interpreter.syscall_signatures[win_version].update(syscall_overrides)

# Define pointer types which are followed automatically
pointer_tracking = ["OBJECT_ATTRIBUTES*", "UNICODE_STRING*", "HANDLE*"]

# Define custom actions on types
custom_handlers = [
    ("UNICODE_STRING", lambda proc, obj: extract_wstring(proc, obj))
]

def decode_windows_data(data):
    """ Tries to decode data read from the Windows target.
    Returns the bytes if no encoding is applicable. """
    for enc in ["utf-16", "utf-8"]:
        try:
            return data.decode(enc)
        except:
            pass
    return data

def extract_wstring(proc, object_representation):
    """ Reads the buffer in a UNICODE_STRING object and
    interprets the content as an utf-16 string. The content is appended
    to the given object.

    Keyword arguments:
    proc                  : tycho process
    object_representation : dictionary with type and value
    """
    value = object_representation[value_tag]
    length = value["Length"][value_tag]
    pointer = value["Buffer"][value_tag]
    content = proc.read_linear(pointer, length)
    object_representation[value_tag]["content"] = decode_windows_data(content)
    return object_representation

def extract_write_file_buffer(proc, parameters):
    """ Reads the buffer passed to the NtWriteFile system call and
    interpretes the content as an utf-8 string. The content is appended
    to the given parameters.

    Keyword arguments:
    proc       : tycho process
    parameters : tycho syscall parameters representation
    """
    pointer = parameters["Buffer"][value_tag]
    length = parameters["Length"][value_tag]
    content = proc.read_linear(pointer, length)
    parameters["Buffer"][ptr_tracked_tag] = {
        "content" : decode_windows_data(content)
    }
    return parameters

autostart_handles = []
autostart_filehandles = []

def get_tracked_value(params):
    tracked_val = params["ObjectAttributes"]["tracked"]["value"]
    return tracked_val["ObjectName"]["tracked"]["value"]["content"]

def handle_syscall_event(fact, proc, ev):
    """ Print information about the name, return value, input and output
    parameters of the system call.

    Keyword arguments:
    fact : system call interpretation factory
    proc : tycho process
    ev   : syscall breakpoint event
    """
    with fact.syscall_interpretation(ev,
                                     pointer_tracking=pointer_tracking,
                                     custom_handlers=custom_handlers) \
                                     as interpreter:
        in_parameters = interpreter.get_parameters()
        name = interpreter.get_syscall_name()

        if name == "NtSetValueKey":
            if in_parameters["KeyHandle"]["value"] in autostart_handles:
                content = in_parameters["ValueName"]["tracked"]["value"]["content"]
                print("Autostart entry created (registry): {}".format(content))
                return True

        if name == "NtWriteFile":
            in_parameters = extract_write_file_buffer(proc, in_parameters)
            if in_parameters["FileHandle"]["value"] in autostart_filehandles:
                content = in_parameters["Buffer"]["tracked"]["content"]
                print("Autostart entry created (file): {}".format(content))
                return True

        out_parameters = interpreter.execute()

        if name == "NtCreateFile":
            val = get_tracked_value(in_parameters)
            handle = out_parameters["FileHandle"]["tracked"]["value"]
            needle = "AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup"
            if needle in val and handle not in autostart_filehandles:
                print("Autostart (file) found! {}, handle {}".format(val, handle))
                autostart_filehandles.append(handle)

        if name in ["NtOpenKey", "NtOpenKeyEx"]:
            val = get_tracked_value(in_parameters)
            handle = out_parameters["pKeyHandle"]["tracked"]["value"]
            if "CurrentVersion\\Run" in val:
                print("Autostart (registry) found! {}, handle {}".format(val, handle))
                autostart_handles.append(handle)

        if name == "NtTerminateProcess":
            print("Process terminating, you should detach the debugger now.")
            return True

        return False

class TychoHook(ida_dbg.DBG_Hooks):
    def dbg_exception(self, pid, tid, ea, exc_code, exc_can_cont, exc_ea, exc_info):
        """
        This hook will be called when a SYSCALL breakpoint hits.
        We will use the system call interpretation to decide whether
        or not to break into IDA. For uninteresting system calls,
        we use the batch mode to automatically continue the process.
        """
        ev = pyTycho.tycho.Event(pyTycho.tycho.SYSCALL_BP, self.proc.open_thread(tid))
        factory = SystemCallInterpretationFactory(self.proc)
        hit = handle_syscall_event(factory, self.proc, ev)

        if hit:
            idc.batch(0)
        else:
            idc.batch(1)
            ida_dbg.continue_process()

        return 0 # Suppress dialog

class autostart_plugin_t(ida_idaapi.plugin_t):
    flags = 0
    comment = "Autostart BP example"
    help = "Semantic breakpoints on startup modifications"
    wanted_name = "Autostart BP"
    wanted_hotkey = "Ctrl-Shift-F9"

    def init(self):
        return ida_idaapi.PLUGIN_OK

    def run(self, arg):
        print("Installing hook...")
        self.debughook = TychoHook()
        self.debughook.hook()
        self.debughook.count = 0
        print("...done.")

        service = pyTycho.Tycho()
        procname = ida_kernwin.ask_str("", ida_kernwin.HIST_CMT, "Process name:")
        self.debughook.proc = service.open_process(procname)
        self.debughook.proc.pause()
        while not self.debughook.proc.is_running():
            print("Process not detected yet...")
            time.sleep(2)

        syscall_whitelist = (
            pyTycho.Tycho.syscalls.NtCreateFile,
            pyTycho.Tycho.syscalls.NtWriteFile,
            pyTycho.Tycho.syscalls.NtOpenKey,
            pyTycho.Tycho.syscalls.NtOpenKeyEx,
            pyTycho.Tycho.syscalls.NtSetValueKey,
            pyTycho.Tycho.syscalls.NtTerminateProcess
        )

        breakpoint = self.debughook.proc.get_syscall_breakpoint()
        for syscall in syscall_whitelist:
            breakpoint.add_syscall_whitelist(syscall)
        breakpoint.enable()

        bitness = 64 if idc.__EA64__ else 32
        self.debughook.proc.launch_gdb_stub(1234, bitness)
        ida_dbg.attach_process(0, -1)

        print("Ready to start sample, press continue now.")

    def term(self):
        pass

def PLUGIN_ENTRY():
    return autostart_plugin_t()

# Autostart semantic breakpoint example

This IDA plugin demonstrates how to combine debugger hooks with
the Tycho Python API to configure semantic breakpoints that trigger
on modifications to the startup entries.

## Prerequisites

The plugin assumes that the remote debugger is correctly setup to
use with the Tycho GDB stub.

## Usage

The plugin asks for a process name to connect to. Afterwards, it
sets up the debugger hooks and configures system call breakpoints
to trigger on file system and registry accesses.

After resuming the process, control will be handed back to IDA
whenever a startup entry is created via one of the following methods:

- A registry entry in a key containing the path segment `CurrentVersion\Run`
- A file where the folder path contains `AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup`

The breakpoint conditions are not an exhaustive list, but rather a demonstration of
how to express those conditions.

After the program is terminated (via the system call `NtTerminateProcess`),
the debugger needs to be detached. Once IDA is closed, the application
will be released so it terminates completely.

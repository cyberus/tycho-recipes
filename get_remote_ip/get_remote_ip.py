#!/usr/bin/env python2

# Tycho recipes - A collection of example scripts using the Tycho Python API
# Copyright (C) 2020 Sebastian Manns, Cyberus Technology GmbH
# <sebastian.manns@cyberus-technology.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import collections
import struct
import socket
import sys
import time

import pyTycho
from pyTycho.syscall_interpreter import SystemCallInterpretationFactory,\
                                        SupportedWindowsVersions

# Define Windows version of the target system
win_version = SupportedWindowsVersions.win7_24150
pyTycho.Tycho.windows_version = win_version

Ipv4Connection = collections.namedtuple("Ipv4Connection",["address","port"])

# This exception is raised if the address family not AF_INET (IPv4).
class UnsupportedAddressFamily(Exception):
    pass


def parse_sockaddr(sockaddr_buffer):
    """
    Returns the address and port described by the buffer as a sockaddr tuple.

    The buffer needs to be of the `SOCKADDR` type as defined in Windows:
    https://docs.microsoft.com/en-us/windows/win32/winsock/sockaddr-2

    Note that currently, only AF_INET (IPv4) is supported.

    Keyword arguments:
    sockaddr_buffer : Buffer containing a complete SOCKADDR structure
    """
    sa_family = struct.unpack("H", sockaddr_buffer[0:2])[0]
    if sa_family != socket.AF_INET:
        raise UnsupportedAddressFamily("Address family {}\
                                        is not supported".format(sa_family))
    ip = socket.inet_ntop(sa_family, sockaddr_buffer[4:8])
    port = struct.unpack(">H", sockaddr_buffer[2:4])[0]
    return Ipv4Connection(ip, port)


def parse_afd_connect(in_params, proc):
    """
    Extracts the remote address passed to IOCTL_AFD_CONNECT.

    The `InputBuffer` parameter of this system call points to a structure
    of the type `AFD_CONNECT_INFO`. This structure contains a member
    `RemoteAddress` of the type `SOCKADDR` at offset 24, which is passed
    to `parse_sockaddr` to extract the IP and port.

    Keyword arguments:
    in_params : Input parameters of NtDeviceIoControlFile system call
    proc      : Tycho process
    """
    input_buffer = in_params["InputBuffer"]["value"]
    input_buffer_length = in_params["InputBufferLength"]["value"]
    afd_connect_info = proc.read_linear(input_buffer, input_buffer_length)
    sockaddr_member = afd_connect_info[24:input_buffer_length]
    return parse_sockaddr(sockaddr_member)


def handle_syscall_event(fact, ev, proc):
    """
    This script handles two system calls:
    - NtDeviceIoControlFile: Print remote address information
      (IP, port, protocol)
    - NtTerminateProcess: Terminate the script

    Keyword arguments:
    fact : system call interpretation factory
    ev   : syscall breakpoint event
    proc : Tycho process
    """
    with fact.syscall_interpretation(ev) as interpreter:
        in_parameters = interpreter.get_parameters()
        name = interpreter.get_syscall_name()

    if name == "NtDeviceIoControlFile":
        afd_connect = 0x12007 # IOCTL for AFD_Connect
        ioctl = in_parameters["IoControlCode"]["value"]
        if ioctl == afd_connect:
            try:
              ip_port = parse_afd_connect(in_parameters, proc)
              try:
                  socket_type = socket.getservbyport(ip_port.port)
              except socket.error:
                  socket_type = "unknown service type"
              print("Open a connection to IP: {}:{} ({})".format(ip_port.address,
                                                                 ip_port.port,
                                                                 socket_type))
            except UnsupportedAddressFamily as e:
                print(e.message)

    if name == "NtTerminateProcess":
        exit(0)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("Usage: python {} <process_name>".format(sys.argv[0]))

    service = pyTycho.Tycho()
    process = service.open_process_by_name(sys.argv[1])
    process.pause()
    while not process.is_running():
        print("process not running")
        time.sleep(0.5)

    breakpoint = process.get_syscall_breakpoint()
    breakpoint.add_syscall_whitelist(pyTycho.Tycho.syscalls.NtDeviceIoControlFile)
    breakpoint.add_syscall_whitelist(pyTycho.Tycho.syscalls.NtTerminateProcess)
    breakpoint.enable()
    factory = SystemCallInterpretationFactory(process)

    while True:
        process.resume()
        event = process.wait_for_breakpoint()
        if event.event_category == event.SYSCALL_BP:
            handle_syscall_event(factory, event, process)
